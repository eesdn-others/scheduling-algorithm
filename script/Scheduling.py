'''
Values and functions for the Scheduling problem
'''
import csv
import glob

NB_NODES_RANGE = [20, 40, 80, 100, 120, 140]# 200] #320, 410, 500]
# EDGE_PROBABILITY = [1, 0.5, 0.25, 0.125, 0.09375, 0.0625]
EDGE_PROBABILITY = [1, 0.75, 0.5, 0.25, 0.125, 0] # 0.109375, 0.09375, 0.0625, 0.03125, 0.015625
K2_DISTRIB = [.75, .5, .25]
# MODELS = ["DSC", "listScheduling", "listSchedulingMaster", "list", "listDelay", "2P", "listNoCapacity"]
MODELS = ["listScheduling", "listSchedulingMaster", "2P", "listNoCapacity"]
CPU_RANGE = list(range(1, 6)) + [10]
FACTOR_RANGE = [0, 0.125, 0.25, 0.5, 1, 2, 3, 4]

def readResults(filename):
	'''
	Return the results of the experiment
	'''
	return readResultsFromDescriptor(open(filename, 'r'))
		
def readResultsFromDescriptor(csvfile):
	r = csv.reader(csvfile, delimiter='\t')
	metrics = {}
	try:
		row = r.next()
		metrics["makespan"] = int(float(row[0]))
	except Exception as e:
		print( "Error", e)

	return metrics

def readInstance(filename):
	'''
	Return the results of the experiment
	'''
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		instances = {}
		try:
			row = r.next()
			instances["nbNodes"] = int(float(row[0]))
			instances["nbEdges"] = int(float(row[1]))
			instances["CPUtimes"] = []
			for _ in range(instances["nbNodes"]):
				row = r.next()
				instances["CPUtimes"].append(int(float(row[0])))
			instances["edges"] = []
			for _ in range(instances["nbEdges"]):
				row = r.next()
				instances["edges"].append([int(float(v)) for v in row])
				assert len(instances["edges"][-1]) == 3

		except Exception as e:
			print( "Error in ", filename, e)

	return instances

def getAvailableInstances(): 
	return [file[len("instances/"):-len(".inst")] for file in glob.glob("instances/*.inst")]
