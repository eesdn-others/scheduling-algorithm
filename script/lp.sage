import sys
import random
import collections
import json
import numpy as np

def ilp(precG, cMachines, nMachines, Km, Kr):
	print "ilp"
	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
	lp.solver_parameter("CPX_PARAM_TILIM", 3600)
	lp.solver_parameter("CPX_PARAM_SYMMETRY", 5)


	C = lp.new_variable(integer=True, nonnegative=True, name = 'C')
	R = lp.new_variable(integer=True, nonnegative=True, name = 'R')
	
	Xc = lp.new_variable(binary=True, name = 'Xc')
	Xr = lp.new_variable(binary=True, name = 'Xr')
	M = lp.new_variable(binary=True, name = 'M')

	timeSlots = range(1, precG.order()+1)
	# Minimize max span
	lp.set_objective(C["max"])

	for i in precG.vertices():
		lp.add_constraint(C[i] == lp.sum([t * Xc[i, m, t] for t in timeSlots for m in cMachines]), name="C{}".format(i))
		lp.add_constraint(C[i] <= C["max"], name="C{} <= Cmax".format(i))
		lp.add_constraint(lp.sum([Xc[i, m, t] for t in timeSlots for m in cMachines]) == 1, name="task {} must be done".format(i))

	for i, j in precG.edges(labels=None):
		lp.add_constraint(lp.sum([Xr[i, j, r, t] for t in timeSlots for r in nMachines + [-1]]) == 1, name="task ({}, {}) must be done".format(i, j))
		lp.add_constraint(R[i, j] == lp.sum([t * Xr[i, j, r, t] for t in timeSlots for r in nMachines + [-1]]), name="task ({}, {}) time".format(i, j))
		
		lp.add_constraint(R[i, j] - C[i] >= lp.sum([Xr[i, j, r, t] for t in timeSlots for r in nMachines]))
		lp.add_constraint(C[j] - R[i, j] >= 1)
		lp.add_constraint(C[j] - C[i] >= 1)

		lp.add_constraint(lp.sum([Xr[i, j, -1, t] for t in timeSlots]) == lp.sum([M[i, j, m] for m in cMachines]))

		
		for m in cMachines:
			lp.add_constraint(0 <= lp.sum([Xc[i, m, t] for t in timeSlots]) + lp.sum([Xc[j, m, t] for t in timeSlots]) - 2 * M[i, j, m] <= 1, name="on same machine {}, {}, {}".format(i, j, m))

	for t in timeSlots:
		for m in cMachines:
			lp.add_constraint( lp.sum([Xc[i, m, t] for i in precG.vertices()]) <= Km[m], name="capaM {}".format(m) )
		for r in nMachines:
			lp.add_constraint( lp.sum([Xr[i, j, r, t] for i, j in precG.edges(labels=None)]) <= Kr[r], name="capaR {}".format(r) )

	makespan = lp.solve(log=1)
	# print "Makespan:", makespan

	# for m in cMachines:
	# 	for i, j in precG.edges(labels=None):
	# 		print "M[{}, {}, {}]={}".format(i, j, m, lp.get_values(M[i, j, m]))
	
	# for i in precG.vertices():
	# 	print "C[{}]={}".format(i, lp.get_values(C[i]))

	# for i, j in precG.edges(labels=None):
	# 	print "R[{}]={}".format((i, j),lp.get_values(R[i, j]))

	# for m in cMachines:
	# 	print m,  "->",
	# 	for t in range(1, makespan+1):
	# 		printed = False
	# 		for i in precG.vertices():
	# 			if lp.get_values(Xc[i, m, t]):
	# 				print i, 
	# 				printed = True
	# 				break
	# 		if not printed:
	# 			print "_",
	# 	print
	# print
	# for r in nMachines:
	# 	print r, "->",
	# 	for t in range(1, makespan+1):
	# 		printed = False
	# 		for i, j in precG.edges(labels=None):
	# 			if lp.get_values(Xr[i, j, r, t]):
	# 				print (i, j), 
	# 				printed = True
	# 				break
	# 		if not printed:
	# 			print "_",
	# 	print
	
	cTasks = {}
	nTasks = {}
	for m in cMachines:
		cTasks[m] = []
		for t in range(1, makespan+1):
			tasks = []
			for i in precG.vertices():
				if(lp.get_values(Xc[i, m, t])):
					tasks.append(i)
			cTasks[m].append(tasks)

	for r in nMachines:
		nTasks[r] = []
		for t in range(1, makespan):
			tasks = []
			for i, j in precG.edges(labels=None):
				if(lp.get_values(Xr[i, j, r, t])):
					tasks.append((i, j))
			nTasks[r].append(tasks)

	return makespan, cTasks, nTasks

# def ilp_v2(precG, cMachines, nMachines, Kc, Kn):
# 	print "ilp_v2"
# 	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
# 	lp.solver_parameter("CPX_PARAM_TILIM", 3600)
# 	lp.solver_parameter("CPX_PARAM_SYMMETRY", 5)

# 	timeSlots = range(1, precG.order()+1)

# 	makespan = lp.new_variable(nonnegative=True, integer=True, name="makespan")

# 	C = lp.new_variable(nonnegative=True, integer=True, name="C")
# 	R = lp.new_variable(nonnegative=True, integer=True, name="r")
	
# 	x = lp.new_variable(binary=True, name="x")
# 	y = lp.new_variable(binary=True, name="y")
# 	m = lp.new_variable(binary=True, name="m")

# 	for i in precG.vertex_iterator():
# 		for j in precG.vertex_iterator():
# 			for k in precG.vertex_iterator():
# 				if i != j and i != k and j != k:
# 					lp.add_constraint( m[i, j] >= m[i, k] + m[k, j] - 1 )
# 					lp.add_constraint( m[i, j] <= m[i, k] + 1 - m[k, j] )
# 					lp.add_constraint( m[i, j] <= m[k, j] + 1 - m[i, k] )

# 	for i in precG.vertex_iterator():
# 		for j in precG.vertex_iterator():
# 			if i != j:
# 				for t in timeSlots:
# 					lp.add_constraint( x[i, t] + x[j, t] <= 2 - m[i, j] )


# 	for i in precG.vertex_iterator():
# 		lp.add_constraint(makespan[0] >= C[i])
# 		lp.add_constraint(C[i] == lp.sum([ t * x[i, t] for t in timeSlots ]))
# 		lp.add_constraint(lp.sum([ x[i, t] for t in timeSlots ]) == 1)

# 		for j in precG.vertex_iterator():
# 			if i != j:
# 				lp.add_constraint(m[i, j] == m[j, i])

# 	for i, j in precG.edge_iterator(labels=None):
# 		lp.add_constraint( C[j] - C[i] >= 2 - m[i, j] )
# 		lp.add_constraint( lp.sum([ y[i, j, t] for t in timeSlots ]) == 1 - m[i, j] )
# 		lp.add_constraint( C[j] - R[i, j] >= 1 )
# 		lp.add_constraint( R[i, j] - C[i] >= 0 )
# 		lp.add_constraint( R[i, j] >= lp.sum([ t * y[i, j, t] for t in timeSlots]) )
# 		for t in timeSlots:
# 			lp.add_constraint( y[i, j, t] <= 1 - x[i, t] )

# 	for t in timeSlots:
# 		lp.add_constraint( lp.sum([ x[i, t] for i in precG.vertex_iterator() ]) <= sum([ Kc[mach] for mach in cMachines]) )
# 		lp.add_constraint( lp.sum([ y[i, j, t] for i, j in precG.edge_iterator(labels=None) ]) <= sum([ Kn[mach] for mach in nMachines]) )

# 	lp.add_constraint(makespan[0] <= precG.order())
	
# 	lp.set_objective(makespan[0])
	
# 	OPTmakespan = lp.solve(log=1)
	

# 	for t in timeSlots:
# 		if t <= OPTmakespan:
# 			print "Time =", t, ":",
# 			for i in precG.vertex_iterator():
# 				if lp.get_values(x[i, t]):
# 					print i, 
# 			print "|",
# 			for i, j in precG.edge_iterator(labels=None):
# 				if lp.get_values(y[i, j, t]):
# 					print (i, j),
# 			print

# 	cTasks = {}
# 	nTasks = {}

# 	print OPTmakespan, cTasks, nTasks

def greedyHeur(precG, cMachines, nMachines, Km, Kr):
	print "greedyHeur"
	done = collections.defaultdict(lambda:False)

	ctm = [[] for _ in cMachines]
	ntm = [[] for _ in nMachines]
	cTasksToBeDone = []
	rTasksToBeDone = []
	machines = {}
	for nTask in precG.vertices():
		if precG.in_degree(nTask) == 0:
			cTasksToBeDone.append(nTask)
	# print cTasksToBeDone		
	while(cTasksToBeDone or rTasksToBeDone):
		#
		# Place first CPU tasks
		#
		for m in cMachines:
			tasks = []
			while len(tasks) < Km[m] and cTasksToBeDone:
				task = cTasksToBeDone.pop()
				# print task, "on", m			
				done[task] = True
				machines[task] = m
				tasks.append(task)
				for edge in precG.outgoing_edges(task, labels=None):
					rTasksToBeDone.append(edge)
			ctm[m].append(tasks)
		#
		# Place network tasks
		#
		for r in nMachines:
			tasks = []
			while(len(tasks) < Kr[r]) and rTasksToBeDone:
				# Select one task to do
				task = rTasksToBeDone.pop()
				# print task, "on", r
				done[task] = True
				tasks.append(task)

				# For the CPU task at the receiving end of the network task
				# Check every preceding network task was done
				canBeDone = True
				for e in precG.incoming_edges(task[1], labels=None):
					if not done[e]:
						# print task[1], "can't be done because of", e
						canBeDone = False
						break
				# If so, it can be done
				if canBeDone:
					cTasksToBeDone.append(task[1])
			ntm[r].append(tasks)
		
		#
		# Place second CPU Tasks
		#
		for m in cMachines:
			tasks = []
			while len(tasks) < Km[m] and cTasksToBeDone:
				task = cTasksToBeDone.pop()
				# print task, "on", m			
				done[task] = True
				machines[task] = m
				tasks.append(task)
				for edge in precG.outgoing_edges(task, labels=None):
					rTasksToBeDone.append(edge)
				# Remove unnecessary network tasks
				for CPUTask1, _ in precG.incoming_edges(task, labels=None):
					if machines[CPUTask1] == machines[task]:
						for r in nMachines:
							for timeSlot in ntm[r]:
								try:
									timeSlot.remove((CPUTask1, task))
									# print "Remove", (CPUTask1, task)
									while(len(tasks) < Kr[r]) and rTasksToBeDone:
										# Select one task to do
										task = rTasksToBeDone.pop()
										# print task, "on", r
										done[task] = True
										timeSlot.append(task)

										# For the CPU task at the receiving end of the network task
										# Check every preceding network task was done
										canBeDone = True
										for e in precG.incoming_edges(task[1], labels=None):
											if not done[e]:
												# print task[1], "can't be done because of", e
												canBeDone = False
												break
										# If so, it can be done
										if canBeDone:
											cTasksToBeDone.append(task[1])
								except:
									pass

			ctm[m].append(tasks)

	tasked = []
	makespan = 0
	for t in range(len(ctm[0])):
		task = False
		for r in nMachines:
			if t < len(ntm[r]) and ntm[r][t]:
				task = True
		for m in cMachines:
			if ctm[m][t]:
				task = True		
		if task:
			makespan += 1
			tasked.append(True)
		else:
			tasked.append(False)

	# print "Makespan:", makespan
	cTasks = {}
	nTasks = {}
	for m in cMachines:		
		# print ctm[m]
		cTasks[m] = []
		for t, b in zip(range(len(ctm[m])), tasked): # Parcours trop long
			# print t, b
			if b:
				cTasks[m].append(ctm[m][t])

	for r in nMachines:
		# print ntm[r]
		nTasks[r] = []
		for t, b in zip(range(len(ntm[r])), tasked): # Parcours trop long
			if b:
				nTasks[r].append(ntm[r][t])
	return makespan, cTasks, nTasks

def greedy(precG, cMachines, nMachines, Km, Kr):
	print "greedy"
	cDone = np.full(precG.order(), False, np.dtype(bool))
	nDone = collections.defaultdict(lambda: False)
	
	cTasks = collections.defaultdict(list)
	nTasks = collections.defaultdict(list)

	machine = {}
	time = {}
 	precDone = np.full((precG.order(), len(cMachines)), 0, np.dtype(int))
	precNetDone = np.full(precG.order(), 0, np.dtype(int))

	cCanBeDone = collections.defaultdict(list)
	nTasksToBeDone = []

	for nTask in precG.vertices():
		if precG.in_degree(nTask) == 0:
			for m in cMachines:
				cCanBeDone[m].append(nTask)

	doneTasks = 0
	t = 0
	while doneTasks < precG.order():
		# for m in cMachines:
		# 	print m, "->", cCanBeDone[m]
		# print nTasksToBeDone

		# Place CPU tasks
		for m in cMachines:
			cTasks[m].append([])
			while len(cTasks[m][t]) < Km[m] and cCanBeDone[m]:
				CPUTask1 = random.choice(cCanBeDone[m])
				cCanBeDone[m].remove(CPUTask1)
				cTasks[m][t].append(CPUTask1)
				machine[CPUTask1] = m
				time[CPUTask1] = t+1
				doneTasks += 1
				cDone[CPUTask1] = True
				# print "Done: ", CPUTask1, "on", m
				
				# Remove useless network tasks from possible pool
				b4 = len(nTasksToBeDone)
				nTasksToBeDone = [(_CPUTask1, _CPUTask2) for (_CPUTask1, _CPUTask2) in nTasksToBeDone if _CPUTask2 != CPUTask1]
				assert b4 >= len(nTasksToBeDone)
				# Remove done CPU tasks 
				for _m in cMachines:
					cCanBeDone[_m] = [ta for ta in cCanBeDone[_m] if not cDone[ta]]

		# Place network tasks
		for r in nMachines:
			nTasks[r].append([])
			while len(nTasks[r][t]) < Kr[r] and nTasksToBeDone:
				nTask = random.choice(nTasksToBeDone)
				nTasksToBeDone.remove(nTask)
				nTasks[r][t].append(nTask)
				assert not cDone[nTask[1]], "Done at time {} on m{} (Now is {})".format(time[nTask[1]], machine[nTask[1]], t+1)
				# print "Done: ", nTask, "on", r

		t += 1
		# Update possible tasks
		for m in cMachines:
			for CPUTask1 in cTasks[m][-1]:
				# Update possible CPU task placement
				# Add task to machine if all precedence done
				for CPUTask2 in precG.neighbors_out(CPUTask1):
					assert not cDone[CPUTask2]
					precDone[CPUTask2][m] += 1
					if precDone[CPUTask2][m] + precNetDone[CPUTask2] == precG.in_degree(CPUTask2):
						cCanBeDone[m].append(CPUTask2)

				# Add network tasks
				for netTask in precG.outgoing_edges(CPUTask1, labels=None):
					assert not cDone[netTask[1]]
					# print "nTasksToBeDone.append({})".format(netTask)
					nTasksToBeDone.append(netTask)

		for r in nMachines:
			for nTask in nTasks[r][-1]:
				nDone[nTask] = True
				# Update possible CPU task pool
				CPUTask1, CPUTask2 = nTask
				assert not cDone[CPUTask2], "Done at time {} on m{} (Now is {})".format(time[CPUTask2], machine[CPUTask2], t)
				precNetDone[CPUTask2] += 1
				precDone[CPUTask2][machine[CPUTask1]] -= 1
				for m in cMachines:
					if precDone[CPUTask2][m] + precNetDone[CPUTask2] == precG.in_degree(CPUTask2) and not cDone[CPUTask2]:
						cCanBeDone[m].append(CPUTask2)
	return t, dict(cTasks), dict(nTasks)

def saveSol(filename, d):
	# print makespan, cTasks, nTasks
	with open(filename, "ab") as f:
		json.dump(d, f)
		f.write("\n")

def createProblem(nbTasks, nbMachinesC, nbMachinesR, p, indKapaM, indKapaR):
	g = DiGraph(nbTasks)
	for i in range(nbTasks):
		for j in range(i, nbTasks):
			if p == 1 or random.random() < p:
				g.add_edge(i, j)
	print "G:", g.order(), g.size()
	for u in g.vertices():
		print u, g.outgoing_edges(u, labels=None)
	for u in g.vertices():
		print u, g.incoming_edges(u, labels=None)

	return g, range(nbMachinesC), range(nbMachinesR), [indKapaM for _ in range(nbMachinesC)], [indKapaR for _ in range(nbMachinesR)]

if __name__ == "__main__":
	nbTasks = int(sys.argv[1])
	nbMachinesC = int(sys.argv[2])
	nbMachinesR = int(sys.argv[3])

	p = float(sys.argv[4])
	g, cMachines, nMachines, Km, Kr = createProblem(nbTasks, nbMachinesC, nbMachinesR, p, 1, 1)

	# if g.size() > 0 and g.order() > 0:
	# 	res = {}
	# 	# makespanG2, cTasksG2, nTasksG2 = greedyHeur(g, cMachines, nMachines, Km, Kr)
	# 	# res["greedy2"] = {"makespan": float(makespanG2), "cTasks": cTasksG2, "nTasks": nTasksG2}
	# 	makespanG, cTasksG, nTasksG = greedy(g, cMachines, nMachines, Km, Kr)
	# 	res["greedy"] = {"makespan": float(makespanG), "cTasks": cTasksG, "nTasks": nTasksG}
	# 	makespanLP, cTasksLP, nTasksLP = ilp(g, cMachines, nMachines, Km, Kr)
	# 	res["lp"] = {"makespan": float(makespanLP), "cTasks": cTasksLP, "nTasks": nTasksLP}
	# 	for m in ["lp", "greedy"]:#, "lp"]:
	# 		print m, res[m]["makespan"]
	# 	saveSol("results/{}_{}_{}_{}.json".format(nbTasks, nbMachinesC, nbMachinesR, p), res)
	# 	# saveSol("results/lp/{}_{}_{}_{}.json".format(nbTasks, nbMachinesC, nbMachinesR, p), *ilp(g, cMachines, nMachines, Km, Kr))
	# 	# saveSol("results/greedy/{}_{}_{}_{}.json".format(nbTasks, nbMachinesC, nbMachinesR, p), *greedyHeur(g, cMachines, nMachines, Km, Kr))
	print ilp(g, cMachines, nMachines, Km, Kr)
	# print ilp_v2(g, cMachines, nMachines, Km, Kr)


