#include <iostream>

#include <MyTimer.hpp>

#include "Scheduling.hpp"
#include "ListScheduling.hpp"


int main(int argc, char** argv) {
	Scheduling::Instance inst = readFromFile("instances/perf1.inst");
	std::clog << "ListScheduling times: ";
	for (uint8_t nbCPU = 0; nbCPU < 100; ++nbCPU) {
		inst.nbCPU = nbCPU;
		Time timer;
		timer.start();
		const auto sol = listScheduling(inst);
		const auto[CPU, Wall] = timer.get();
		std::cout << nbCPU << "(" << CPU << ", " << Wall << ")\t";
	}
	return 0;
}