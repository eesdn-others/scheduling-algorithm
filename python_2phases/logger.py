import logging

from definitions import *


class Logger():
    def __init__(self, instance_id, max_cpu_machines, network_factor=None, enabled=False):
        if network_factor:
            log_path = os.path.join(ROOT_DIR,
                                    "log/log_id_{}_{}_{}.log".format(instance_id, max_cpu_machines, network_factor))
            self.log_path = log_path
        else:
            log_path = os.path.join(ROOT_DIR, "log/log_id_{}_cpu{}.log".format(instance_id, max_cpu_machines))
            self.log_path = log_path

        logger = logging.getLogger(str(instance_id))
        logger.setLevel(logging.INFO)
        fh = logging.FileHandler(log_path, mode="w")
        formatter = logging.Formatter('%(asctime)s \t %(message)s', datefmt='%a, %d %b %Y %H:%M:%S', )
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        self._logger = logger
        if not enabled:
            self._logger.disabled = True

    def log(self, str):
        self._logger.info(str)

    def delete(self):
        os.remove(self.log_path)
