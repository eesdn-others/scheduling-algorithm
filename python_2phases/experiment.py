from definitions import *
from instance import Instance
from k_partition import Partition
from logger import Logger
from schedule import Schedule


class Experiment:
    def __init__(self):
        pass

    def run_all_exp_in_dir(self, path, max_cpu=10, method=1):

        for file in os.listdir(path):
            if file.endswith(".inst"):
                file_path = os.path.join(path, file)

                print(file_path)
                instance_id, ext = file.split(".")

                logger = Logger(instance_id, max_cpu)
                instance = Instance(instance_id, max_cpu, logger, file_path)
                # instance.draw_graph()

                partition = Partition(instance, logger)
                n_cpu, partitions = partition.get_best_partition(method=method)

                schedule = Schedule(instance, partitions, logger)
                schedule.schedule_by_layer()
                # schedule.output(to_excel=True)

                del logger
