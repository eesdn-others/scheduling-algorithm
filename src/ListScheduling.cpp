#include "ListScheduling.hpp"
#include <range/v3/all.hpp>

Scheduling::Solution listDelay(const Scheduling::Instance& _inst) {
    std::vector<Graph::Node> taskAssignment(_inst.workflow.getOrder(), std::numeric_limits<int>::max());

    std::vector<int> machineCompletion(_inst.nbCPU, 0);
    boost::multi_array<int, 2> minStartTime(boost::extents[_inst.nbCPU][_inst.workflow.getOrder()]);
    std::fill_n(minStartTime.data(), minStartTime.num_elements(), 0.0);

    std::vector<int> notDonePredecessors = getInDegrees(_inst.workflow);
    const auto allTasks = ranges::view::ints(0, _inst.workflow.getOrder());
    const auto allMachines = ranges::view::ints(0, _inst.nbCPU);

    std::vector<int> availableTasks = allTasks | ranges::view::remove_if([&](auto&& _task) {
        return notDonePredecessors[_task] > 0 
            || taskAssignment[_task] != std::numeric_limits<int>::max();});
    
    auto possibleAssignment = ranges::view::cartesian_product(allMachines, availableTasks);

    const auto hasSmallestStart = [&](auto&& _tm1, auto&& _tm2) {
        return minStartTime[std::get<0>(_tm1)][std::get<1>(_tm1)] 
            < minStartTime[std::get<0>(_tm2)][std::get<1>(_tm2)];
    };
    
    while(!possibleAssignment.empty()) {
        const auto pair = *ranges::min_element(possibleAssignment, hasSmallestStart);

        const auto m = std::get<0>(pair);
        const auto task = std::get<1>(pair);
        const auto taskStart = std::max(machineCompletion[m], minStartTime[m][task]);
        // std::cout << "Placing " << task << " on machine " << m 
        //     << " starting from " << taskStart << "\n";

        machineCompletion[m] = taskStart + _inst.times[task];
        taskAssignment[task] = m;

        // Update minStartTime of every successor
        for(const auto succ : _inst.workflow.getNeighbors(task)) {
            notDonePredecessors[succ]--;
            for(const auto m1 : allMachines) {
                minStartTime[m1][succ] = std::max<int>({minStartTime[m1][succ], 
                    taskStart + _inst.times[task] + _inst.workflow.getEdgeWeight(task, succ)});
                // std::cout << "minStartTime["<<m1<<"]["<<succ<<"]: " << minStartTime[m1][succ] << '\n';
            }
        }
        availableTasks = allTasks | ranges::view::remove_if([&](auto&& _task) {
            return notDonePredecessors[_task] > 0 
                || taskAssignment[_task] != std::numeric_limits<int>::max();});
        
        // Update minStartTime on m
        for(const auto j : availableTasks) {
            minStartTime[m][j] = std::max(minStartTime[m][j], machineCompletion[m]);
            // std::cout << "minStartTime["<<m<<"]["<<j<<"]: " << minStartTime[m][j] << '\n';
        }

        possibleAssignment = ranges::view::cartesian_product(allMachines, availableTasks);
    }
    std::cout << "Fake makespan: " 
        << *std::max_element(machineCompletion.begin(), machineCompletion.end()) << '\n';
    return schedulingFromMachineAssigment(taskAssignment, _inst);
}

Scheduling::Solution listNoCapacity(const Scheduling::Instance& _inst) {
    std::vector<Graph::Node> taskAssignment(_inst.workflow.getOrder(), std::numeric_limits<int>::max());

    std::vector<int> machineCompletion(_inst.nbCPU, 0);
    boost::multi_array<int, 2> minStartTime(boost::extents[_inst.nbCPU][_inst.workflow.getOrder()]);
    std::fill_n(minStartTime.data(), minStartTime.num_elements(), 0.0);

    std::vector<int> notDonePredecessors = getInDegrees(_inst.workflow);
    const auto allTasks = ranges::view::ints(0, _inst.workflow.getOrder());
    const auto allMachines = ranges::view::ints(0, _inst.nbCPU);

    std::vector<int> availableTasks = allTasks | ranges::view::remove_if([&](auto&& _task) {
        return notDonePredecessors[_task] > 0 
            || taskAssignment[_task] != std::numeric_limits<int>::max();});
    
    auto possibleAssignment = ranges::view::cartesian_product(allMachines, availableTasks);

    const auto hasSmallestStart = [&](auto&& _tm1, auto&& _tm2) {
        return minStartTime[std::get<0>(_tm1)][std::get<1>(_tm1)] 
            < minStartTime[std::get<0>(_tm2)][std::get<1>(_tm2)];
    };
    
    while(!possibleAssignment.empty()) {
        const auto pair = *ranges::min_element(possibleAssignment, hasSmallestStart);

        const auto m = std::get<0>(pair);
        const auto task = std::get<1>(pair);
        const auto taskStart = std::max(machineCompletion[m], minStartTime[m][task]);
        // std::cout << "Placing " << task << " on machine " << m 
        //     << " starting from " << taskStart << "\n";

        machineCompletion[m] = taskStart + _inst.times[task];
        taskAssignment[task] = m;

        // Update minStartTime of every successor
        for(const auto succ : _inst.workflow.getNeighbors(task)) {
            notDonePredecessors[succ]--;
            for(const auto m1 : allMachines) {
                if(m1 != m) {
                    minStartTime[m1][succ] = std::max<int>({minStartTime[m1][succ], 
                        taskStart + _inst.times[task] + _inst.workflow.getEdgeWeight(task, succ)});
                } else {
                    minStartTime[m1][succ] = std::max<int>({minStartTime[m1][succ], 
                        taskStart + _inst.times[task]});
                }
                // std::cout << "minStartTime["<<m1<<"]["<<succ<<"]: " << minStartTime[m1][succ] << '\n';
            }
        }
        availableTasks = allTasks | ranges::view::remove_if([&](auto&& _task) {
            return notDonePredecessors[_task] > 0 
                || taskAssignment[_task] != std::numeric_limits<int>::max();});
        
        // Update minStartTime on m
        for(const auto j : availableTasks) {
            minStartTime[m][j] = std::max(minStartTime[m][j], machineCompletion[m]);
            // std::cout << "minStartTime["<<m<<"]["<<j<<"]: " << minStartTime[m][j] << '\n';
        }

        possibleAssignment = ranges::view::cartesian_product(allMachines, availableTasks);
    }
    std::cout << "Fake makespan: " 
        << *std::max_element(machineCompletion.begin(), machineCompletion.end()) << '\n';
    return schedulingFromMachineAssigment(taskAssignment, _inst);
}

Scheduling::Solution list(const Scheduling::Instance& _inst) {
    std::vector<Graph::Node> taskAssignment(_inst.workflow.getOrder(), std::numeric_limits<int>::max());

    std::vector<int> machineCompletion(_inst.nbCPU, 0);
    boost::multi_array<int, 2> minStartTime(boost::extents[_inst.nbCPU][_inst.workflow.getOrder()]);
    std::fill_n(minStartTime.data(), minStartTime.num_elements(), 0.0);

    std::vector<int> notDonePredecessors = getInDegrees(_inst.workflow);
    const auto allTasks = ranges::view::ints(0, _inst.workflow.getOrder());
    const auto allMachines = ranges::view::ints(0, _inst.nbCPU);

    std::vector<int> availableTasks = allTasks | ranges::view::remove_if([&](auto&& _task) {
        return notDonePredecessors[_task] > 0 
            || taskAssignment[_task] != std::numeric_limits<int>::max();});
    
    auto possibleAssignment = ranges::view::cartesian_product(allMachines, availableTasks);

    const auto hasSmallestStart = [&](auto&& _tm1, auto&& _tm2) {
        return minStartTime[std::get<0>(_tm1)][std::get<1>(_tm1)] 
            < minStartTime[std::get<0>(_tm2)][std::get<1>(_tm2)];
    };
    
    while(!possibleAssignment.empty()) {
        const auto pair = *ranges::min_element(possibleAssignment, hasSmallestStart);

        const auto m = std::get<0>(pair);
        const auto task = std::get<1>(pair);
        const auto taskStart = std::max(machineCompletion[m], minStartTime[m][task]);
        // std::cout << "Placing " << task << " on machine " << m 
        //     << " starting from " << taskStart << "\n";

        machineCompletion[m] = taskStart + _inst.times[task];
        taskAssignment[task] = m;

        // Update minStartTime of every successor
        for(const auto succ : _inst.workflow.getNeighbors(task)) {
            notDonePredecessors[succ]--;
            for(const auto m1 : allMachines) {
                minStartTime[m1][succ] = std::max<int>({minStartTime[m1][succ], 
                        taskStart + _inst.times[task]});                
            }
        }
        availableTasks = allTasks | ranges::view::remove_if([&](auto&& _task) {
            return notDonePredecessors[_task] > 0 
                || taskAssignment[_task] != std::numeric_limits<int>::max();});
        
        // Update minStartTime on m
        for(const auto j : availableTasks) {
            minStartTime[m][j] = std::max(minStartTime[m][j], machineCompletion[m]);
            // std::cout << "minStartTime["<<m<<"]["<<j<<"]: " << minStartTime[m][j] << '\n';
        }

        possibleAssignment = ranges::view::cartesian_product(allMachines, availableTasks);
    }
    std::cout << "Fake makespan: " 
        << *std::max_element(machineCompletion.begin(), machineCompletion.end()) << '\n';
    return schedulingFromMachineAssigment(taskAssignment, _inst);
}

Scheduling::Solution listScheduling(const Scheduling::Instance& _inst) {
    int maxTime = std::accumulate(_inst.times.begin(), _inst.times.end(), 0ul);
    for (const auto& edge : _inst.workflow.getEdges()) {
        maxTime += _inst.workflow.getEdgeWeight(edge);
    }

    Scheduling::Solution sol(_inst);
    std::vector<int> machineCompletion(_inst.nbCPU, 0);
    std::vector<int> taskStarts(_inst.workflow.getOrder(), std::numeric_limits<int>::max());
    std::vector<int> taskAssignment(_inst.workflow.getOrder(), std::numeric_limits<int>::max());
    boost::multi_array<int, 2> minStartTime(boost::extents[_inst.workflow.getOrder()][_inst.nbCPU]);
    std::fill(minStartTime.data(), minStartTime.data() + minStartTime.num_elements(), 0); // Minimum starting time of task j on CPU k counting network tasks
    std::vector<int> inDegrees = getInDegrees(_inst.workflow);
    std::vector<int> longestBranchs = getPrio(_inst);
    std::vector<std::pair<int, int>> L;
    std::vector<int> A;
    for (Graph::Node u = 0; u < _inst.workflow.getOrder(); ++u) {
        if (inDegrees[u] == 0) {
            A.push_back(u);
        }
    }
    boost::multi_array<std::vector<int>, 2> exteMachinePred(boost::extents[_inst.workflow.getOrder()][_inst.nbCPU]);

    int currentT = 0;
    int taskScheduled = 0;
    while (!A.empty()) {
        DBOUT(
            std::cout << '\n';
            sol.printScheduling(std::cout);
        )
        L.clear();

        /* Build list of task/machine pairs */
        const auto& sortValue = [&](const std::pair<int, int>& __p) {
            const auto& reqTasks = exteMachinePred[__p.first][__p.second];
            return std::make_tuple(
                std::accumulate(reqTasks.begin(), reqTasks.end(), 0,
                    [&](auto&& ___s, auto&& ___p) {
                        return ___s + _inst.workflow.getEdgeWeight(___p, __p.first);
                    }),
                longestBranchs[__p.first]);
        };
        for (const auto& task : A) {
            for (int k = 0; k < _inst.nbCPU; ++k) {
                if (minStartTime[task][k] <= currentT) {
                    L.emplace_back(task, k);
                }
            }
        }

        std::sort(L.begin(), L.end(), [&](const auto& __p1, const auto& __p2) {
            return sortValue(__p1) < sortValue(__p2);
        });

        /************************************/

        // bool placed = false;
        for (const auto& pair : L) {
            const int taskID = pair.first,
                      machineID = pair.second;
            // Find valid routing scheduling
            std::vector<int> comStarts;
            comStarts.reserve(exteMachinePred[taskID][machineID].size());

            int startTime = minStartTime[taskID][machineID];
            if (!exteMachinePred[taskID][machineID].empty()) {
                // Try to place every network comm greedily
                for (const auto precedingTask : exteMachinePred[taskID][machineID]) {
                    const auto state = sol.findFirstContiguousSlot({taskAssignment[precedingTask], machineID}, _inst.workflow.getEdgeWeight(precedingTask, taskID),
                        taskStarts[precedingTask] + _inst.times[precedingTask], currentT);

                    if (state.found) { // Found enough continous slots
                        comStarts.push_back(state.comStart);
                        startTime = std::max(
                            startTime, state.comStart + _inst.workflow.getEdgeWeight(precedingTask, taskID));
                        sol.scheduleCommTask({precedingTask, taskAssignment[precedingTask]}, {taskID, machineID}, state.comStart);
                    } else {
                        // No cont. slots, need to change minStartTime
                        minStartTime[taskID][machineID] = std::max<int>(
                            minStartTime[taskID][machineID],
                            currentT + _inst.workflow.getEdgeWeight(precedingTask, taskID) - state.nbContiguousSlotAtEnd);
                        break;
                    }
                }
            }

            if (comStarts.size() == exteMachinePred[taskID][machineID].size()) { // Found a valid network scheduling for every network tasks
                // Place task on CPU
                ++taskScheduled;
                taskAssignment[taskID] = machineID;
                taskStarts[taskID] = std::max(startTime, machineCompletion[machineID]);
                // int lastTime = machineCompletion[machineID];
                machineCompletion[machineID] = taskStarts[taskID] + _inst.times[taskID];
                for (int task2ID = 0; task2ID < _inst.workflow.getOrder(); ++task2ID) {
                    // minStartTime for taskID is also updated because otherwise it would add a superfluous if statement
                    minStartTime[task2ID][machineID] =
                        std::max(minStartTime[task2ID][machineID], machineCompletion[machineID]);
                }

                sol.scheduleCPUTask(taskID, machineID, taskStarts[taskID]);
                A.erase(std::remove(A.begin(), A.end(), taskID), A.end());

                for (const auto& w : _inst.workflow.getNeighbors(taskID)) {
                    if (--inDegrees[w] == 0) { // w can be done because all preceding are placed
                        A.push_back(w);
                    }
                    for (int machine2ID = 0; machine2ID < _inst.nbCPU; ++machine2ID) {
                        if (machine2ID == machineID) {
                            minStartTime[w][machine2ID] =
                                std::max(machineCompletion[machine2ID], minStartTime[w][machine2ID]);
                        } else {
                            minStartTime[w][machine2ID] = std::max(
                                minStartTime[w][machine2ID],
                                std::max(machineCompletion[machine2ID],
                                    machineCompletion[taskAssignment[taskID]] + int(_inst.workflow.getEdgeWeight(taskID, w))));
                            exteMachinePred[w][machine2ID].push_back(taskID);
                        }
                    }
                }
                break;
            } else {
                // Revert communication tasks
                for (int i = 0; i < comStarts.size(); ++i) {
                    const auto& precedingTask = exteMachinePred[taskID][machineID][i];
                    sol.revertScheduleCommTask({precedingTask, taskAssignment[precedingTask]}, {taskID, machineID}, comStarts[i]);
                }
            }
        }

        int t = maxTime;
        for (const auto& taskID : A) {
            for (int k = 0; k < _inst.nbCPU; ++k) {
                t = std::min(t, minStartTime[taskID][k]);
            }
        }
        currentT = t;
        std::cout << '\r' << std::setw(3) << taskScheduled << '/' << std::setw(3)
                  << _inst.workflow.getOrder() << " -> " << std::setw(10)
                  << double(taskScheduled) / _inst.workflow.getOrder() << std::flush;
    }
    std::cout << '\n';
    return sol;
}
