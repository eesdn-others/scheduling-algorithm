#include "Scheduling.hpp"
#include <range/v3/all.hpp>

#include <boost/format.hpp>

namespace Scheduling {

Instance::Instance(DiGraph<int> _workflow, std::vector<int> _times, const int _nbCPU)
    : workflow(std::move(_workflow))
    , times(std::move(_times))
    , nbCPU(_nbCPU)
    , edgeId([&]() {
        boost::multi_array<int, 2> edgeId_(boost::extents[workflow.getOrder()][workflow.getOrder()]);
        std::fill(edgeId_.data(), edgeId_.data() + edgeId_.num_elements(), -1);
        int i = 0;
        for (const auto& edge : workflow.getEdges()) {
            edgeId_[edge.first][edge.second] = i;
            DBOUT(std::cout << "edgeId_[" << edge.first << "][" << edge.second << "]= " << i << '\n';)
            ++i;
        }
        return edgeId_;
    }())
    , lowerBound(static_cast<int>(std::ceil(std::accumulate(times.begin(), times.end(), 0u) / nbCPU)))
    , upperBound([&] {
        double retval = std::accumulate(times.begin(), times.end(), 0ul);
        for (const auto& edge : workflow.getEdges()) {
            retval += workflow.getEdgeWeight(edge);
        }
        return static_cast<int>(2 * retval);
    }()) {}

Solution::Solution(const Scheduling::Instance& _inst)
    : inst(&_inst)
    , machines(inst->nbCPU)
    , inNetwork(inst->nbCPU)
    , outNetwork(inst->nbCPU)
{}

void Solution::scheduleCPUTask(const int _task, const int _machine, const int _time) noexcept {
    DBOUT(std::cout << "scheduling " << _task << " on " << _machine << " starting from " << _time << '\n';)
    const int endOfTask = _time + inst->getCPUCost(_task);
    machines[_machine][_time] = {_task, inst->getCPUCost(_task)};
    makespan = std::max(makespan, endOfTask);
}

Solution Solution::reverse(const Instance& _inst) const {
    Solution reversedSol(_inst);
    reversedSol.makespan = makespan;
    for(int k = 0; k < machines.size(); ++k) {
        for(const auto& [time, task] : machines[k]) {
            reversedSol.machines[k][makespan - (time+task.duration)] = task;
        }
    }
    for(int k = 0; k < inNetwork.size(); ++k) {
        for(const auto& [time, task] : inNetwork[k]) {
            const auto [u, v] = inst->workflow.getEdges()[task.id];
            const auto id = inst->edgeId[u][v];
            reversedSol.inNetwork[k][makespan - (time+task.duration)] = {id, task.duration};
        }
    }
    for(int k = 0; k < outNetwork.size(); ++k) {
        for(const auto& [time, task] : outNetwork[k]) {
            const auto [u, v] = inst->workflow.getEdges()[task.id];
            const auto id = inst->edgeId[u][v];
            reversedSol.outNetwork[k][makespan - (time+task.duration)] = {id, task.duration};
        }
    }
    return reversedSol;
}

void Solution::scheduleCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
    const int _time) noexcept {
    DBOUT(std::cout << "scheduling (" << _task1.task << ", " << _task2.task << ") between out: " << _task1.machine << " and in: " << _task2.machine << " starting from " << _time << '\n';)
    setCommTask(_task1, _task2, _time, inst->edgeId[_task1.task][_task2.task]);
}

void Solution::revertScheduleCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
    const int _time) noexcept {
    DBOUT(std::cout << "revert scheduling (" << _task1.task << ", " << _task2.task << ") between out: " << _task1.machine << " and in: " << _task2.machine << " starting from " << _time << '\n';)
    removeCommTask(_task1, _task2, _time);
}

/**
* Search for contiguous slot from _start to _end on _networkMachines
*/
Solution::ContiguousState Solution::findFirstContiguousSlot(
        const std::pair<int, int>& _networkMachines, const int _size,
        const int _start, const int _end) {
    DBOUT(printScheduling(std::cout);)
    DBOUT(std::cout << "Searching slot of size "<<_size<<" between "<<_start<<" and "<<_end
        <<" in: "<<_networkMachines.second<<", out: "<<_networkMachines.first<<"\n";)
    if (_size == 0) {
        return {true, _start, 0};
    }
    
    const auto inMachine = inNetwork[_networkMachines.second];
    const auto outMachine = outNetwork[_networkMachines.first];

    int start = _start;
    const auto isValidSpacing = [&](auto&& _pair1, auto&& _pair2) {
        const auto firstFreeSlot = std::max(_pair1.first + _pair1.second.duration, start);
        const auto firstNotFreeSlot = std::min(_pair2.first, _end);
        const auto space = firstNotFreeSlot - firstFreeSlot;
        return space >= _size;
    };

    bool changed = true;
    const auto getValidSpacing = [&](auto&& _machine, auto&& _ite){
        // Get valid spacing for out machines
        auto ite = std::adjacent_find(_machine.begin(), _machine.end(), isValidSpacing);
        if(_ite != ite) {
            changed = true;
        }
        if(ite != _machine.end()) {  // We found a valid pair of task to schedule in between
            start = std::max(ite->first + ite->second.duration, start);
        } else if(!_machine.empty()) { // We're at the end
            start = std::max(_machine.rbegin()->first + _machine.rbegin()->second.duration, start);
        }
        DBOUT(std::cout<<"start: "<<start<<'\n';)
        return ite;
    };

    auto outIte = getValidSpacing(outMachine, outMachine.begin());
    auto inIte = getValidSpacing(inMachine, inMachine.begin());
    while(changed) {
        changed = false;
        DBOUT(
            std::cout << "out: ";
            if(outIte != outMachine.end()) {
                std::cout  << outIte->first << '\n';
            } else {
                std::cout << "end\n";
            }
            std::cout << "int: ";
            if(inIte != inMachine.end()) {
                std::cout  << inIte->first << '\n';
            } else {
                std::cout << "end\n";
            }
        )
        
        inIte = getValidSpacing(inMachine, inIte);
        outIte = getValidSpacing(outMachine, outIte);
    }
    DBOUT(
        std::cout << "out: ";
        if(outIte != outMachine.end()) {
            std::cout  << outIte->first << '\n';
        } else {
            std::cout << "end\n";
        }
        std::cout << "int: ";
        if(inIte != inMachine.end()) {
            std::cout  << inIte->first << '\n';
        } else {
            std::cout << "end\n";
        }
    )
    DBOUT(std::cout<<"start: "<<start<<'\n';)

    const auto findFreeSlot = [&](auto&& _machine, auto&& _lastIte) {
        if(_lastIte == _machine.end()) {
            if(_machine.empty()) {
                return _start;
            } else {
                const auto ite = _machine.rbegin();
                return std::max(ite->first + ite->second.duration, start);
            }
        } else {
            return std::max(_lastIte->first + _lastIte->second.duration, start);
        }
    };

    const auto firstOutTimeSlot = findFreeSlot(outMachine, outIte);
    const auto firstInTimeSlot = findFreeSlot(inMachine, inIte);
    const auto possibleStart = std::max(firstOutTimeSlot, firstInTimeSlot);
    if(_end - possibleStart < _size) {
        const auto contiguousSlotAtEnd = _end - possibleStart;
        DBOUT(std::cout << "Couldn't find free slot, slot at end "<<contiguousSlotAtEnd<<" slots\n";)
        return {false, 0, contiguousSlotAtEnd};
    } else {
        DBOUT(std::cout << "Found free slot, starting at "<< possibleStart <<" slots\n";)
        return {true, possibleStart, 0};
    }
}

/**
* Search for contiguous slot from _start on _networkMachines
* Expand the machine if more time is needed
*/
Solution::ContiguousState Solution::findFirstContiguousSlot(
        const std::pair<int, int>& _networkMachines, const int _size,
        int _start) {
    DBOUT(printScheduling(std::cout);)
    DBOUT(std::cout << "Searching slot of size "<<_size<<" from "<<_start
        <<" in: "<<_networkMachines.second<<", out: "<<_networkMachines.first<<"\n";)
    if (_size == 0) {
        return {true, _start, 0};
    }
    
    const auto inMachine = inNetwork[_networkMachines.second];
    const auto outMachine = outNetwork[_networkMachines.first];

    int start = _start;
    const auto isValidSpacing = [&](auto&& _pair1, auto&& _pair2) {
        const auto firstFreeSlot = std::max(_pair1.first + _pair1.second.duration, start);
        const auto firstNotFreeSlot = _pair2.first;
        const auto space = firstNotFreeSlot - firstFreeSlot;
        return space >= _size;
    };

    bool changed = true;
    const auto getValidSpacing = [&](auto&& _machine, auto&& _ite){
        // Get valid spacing for out machines
        auto ite = std::adjacent_find(_machine.begin(), _machine.end(), isValidSpacing);
        if(_ite != ite) {
            changed = true;
        }
        if(ite != _machine.end()) {  // We found a valid pair of task to schedule in between
            start = std::max(ite->first + ite->second.duration, start);
        } else if(!_machine.empty()) { // We're at the end
            start = std::max(_machine.rbegin()->first + _machine.rbegin()->second.duration, start);
        }
        DBOUT(std::cout<<"start: "<<start<<'\n';)
        return ite;
    };

    auto outIte = getValidSpacing(outMachine, outMachine.begin());
    auto inIte = getValidSpacing(inMachine, inMachine.begin());
    while(changed) {
        changed = false;
        DBOUT(
            std::cout << "out: ";
            if(outIte != outMachine.end()) {
                std::cout  << outIte->first << '\n';
            } else {
                std::cout << "end\n";
            }
            std::cout << "int: ";
            if(inIte != inMachine.end()) {
                std::cout  << inIte->first << '\n';
            } else {
                std::cout << "end\n";
            }
        )
        
        inIte = getValidSpacing(inMachine, inIte);
        outIte = getValidSpacing(outMachine, outIte);
    }
    DBOUT(
        std::cout << "out: ";
        if(outIte != outMachine.end()) {
            std::cout  << outIte->first << '\n';
        } else {
            std::cout << "end\n";
        }
        std::cout << "int: ";
        if(inIte != inMachine.end()) {
            std::cout  << inIte->first << '\n';
        } else {
            std::cout << "end\n";
        }
    )
    DBOUT(std::cout<<"start: "<<start<<'\n';)

    const auto findFreeSlot = [&](auto&& _machine, auto&& _lastIte) {
        if(_lastIte == _machine.end()) {
            if(_machine.empty()) {
                return _start;
            } else {
                const auto ite = _machine.rbegin();
                return std::max(ite->first + ite->second.duration, start);
            }
        } else {
            return std::max(_lastIte->first + _lastIte->second.duration, start);
        }
    };

    const auto firstOutTimeSlot = findFreeSlot(outMachine, outIte);
    const auto firstInTimeSlot = findFreeSlot(inMachine, inIte);
    const auto possibleStart = std::max(firstOutTimeSlot, firstInTimeSlot);
    
    DBOUT(std::cout << "Found free slot, starting at "<< possibleStart <<" slots\n";)
    return {true, possibleStart, 0};
    
}

/**
	*	\brief Save the makespan and the scheduling into a file
		\param _filename The filename of the file to save the solution
	*	\sa Solution::printScheduling 
	*/
void Solution::save(const std::string& _filename) const {
    std::ofstream ofs(_filename);
    ofs << makespan << '\n';
    printScheduling(ofs);
    std::cout << "Saved to " << _filename << '\n';
}

void Solution::printScheduling(std::ostream& _out) const noexcept {
    _out << "# CPU\n";
    printScheduling(_out, machines, [](auto&& _id) -> int {
        return _id;
    });
    const auto edges = inst->workflow.getEdges();
    const auto idToEdge = [&](auto&& _id) -> std::string {
        const auto [u, v] = edges[_id];
        return '(' + std::to_string(u) + ", " + std::to_string(v) + ')';
    };
    _out << "# Incoming\n";
    printScheduling(_out, inNetwork, idToEdge);
    _out << "# Outgoing\n";
    printScheduling(_out, outNetwork, idToEdge);
}

/**
*   \brief{Return the placement of the tasks (id, machine and starting time)}
*   \sa TaskPlacement    
*/
std::vector<Solution::TimedTaskPlacement> Solution::getTaskPlacements(
        const std::vector<std::map<int, Task>>& _machine, const int _nbTasks) const noexcept {   
    std::vector<Solution::TimedTaskPlacement> retval(_nbTasks);
    for (int machineID = 0; machineID < inst->nbCPU; ++machineID) {
        for(const auto [time, task] : _machine[machineID]) {
            retval[task.id] = {task.id, machineID, time};
        }
    }
    return retval;
}

/**
    *	\brief Check if the solution is valid for the Instance _inst
	*	\return true if the solution is valid, false otherwise
    */
bool Solution::isValid() const noexcept {
    // Check that all machine are scheduled
    const auto cpuTaskPlacement = getTaskPlacements(machines, inst->workflow.getOrder());
    for (int task = 0; task < inst->workflow.getOrder(); ++task) {
        if (!cpuTaskPlacement[task].isValid()) {
            DBOUT(std::cerr << task << " is not scheduled on any machine\n");
            return false;
        }
    }

    const auto inTaskPlacement = getTaskPlacements(inNetwork, inst->workflow.size());
    const auto outTaskPlacement = getTaskPlacements(outNetwork, inst->workflow.size());
    int e = 0;
    for (const auto & [ task1, task2 ] : inst->workflow.getEdges()) {
        if (cpuTaskPlacement[task1].time + inst->times[task1] > cpuTaskPlacement[task2].time) {
            DBOUT(std::cerr << task2 << " starts before " << task1 << " finishes\n");
            return false;
        }
        if (cpuTaskPlacement[task1].machine != cpuTaskPlacement[task2].machine
            && inst->workflow.getEdgeWeight(task1, task2)) { // Null communications do no need to be scheduled
            if (!inTaskPlacement[e].isValid() || !outTaskPlacement[e].isValid()) {
                DBOUT(std::cerr << "Communication between " << task1 << " and " << task2 << " is not scheduled\n");
                return false;
            }
            if (inTaskPlacement[e].time != outTaskPlacement[e].time) {
                DBOUT(std::cerr << "In&out communication between " << task1 << " and " << task2 << " are not at the same time\n");
                return false;
            }
            if (outTaskPlacement[e].machine != cpuTaskPlacement[task1].machine) {
                DBOUT(std::cerr << "Communication of " << task1 << " comes from the wrong machine\n");
                return false;
            }
            if (inTaskPlacement[e].machine != cpuTaskPlacement[task2].machine) {
                DBOUT(std::cerr << "Communication of " << task2 << " goes to the wrong machine\n");
                return false;
            }
            if (cpuTaskPlacement[task1].time + inst->getCPUCost(task1) > outTaskPlacement[e].time) {
                DBOUT(std::cerr << "Communication between " << task1 << " and " << task2 << " starts before " << task1 << " finishes\n");
                return false;
            }
            if (inTaskPlacement[e].time + inst->workflow.getEdgeWeight(task1, task2) > cpuTaskPlacement[task2].time) {
                DBOUT(std::cerr << boost::format("Communication between %1% and %2% finishes (%3%) after %2% starts (%4%) \n") 
                	%  task1 % task2 % (inTaskPlacement[e].time + inst->workflow.getEdgeWeight(task1, task2)) % cpuTaskPlacement[task2].time);
                return false;
            }
        }
        ++e;
    }
    return true;
}

void Solution::setCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
    const int _time, int _id) noexcept {
    assert(outNetwork[_task1.machine].find(_time) == outNetwork[_task1.machine].end());
    outNetwork[_task1.machine][_time] = {_id, inst->workflow.getEdgeWeight(_task1.task, _task2.task)};

    assert(inNetwork[_task2.machine].find(_time) == inNetwork[_task2.machine].end());
    inNetwork[_task2.machine][_time] = {_id, inst->workflow.getEdgeWeight(_task1.task, _task2.task)};
}

void Solution::removeCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
    const int _time) noexcept {
    outNetwork[_task1.machine].erase(_time);
    inNetwork[_task2.machine].erase(_time);
}

/*Solution Solution::getReverse(const Instance& _reverseInstance) const {
    Scheduling retval(*this);
    retval.inst = &_reverseInstance;
    std::reverse(retval.machines.begin(), retval.machines.end());
    std::reverse(retval.inNetwork.begin(), retval.inNetwork.end());
    std::reverse(retval.outNetwork.begin(), retval.outNetwork.end());
    return retval;
}*/

Scheduling::Instance readFromFile(const std::string& _filename, int _nbPCU) {
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        throw std::invalid_argument(_filename);
    }
    int nbJobs, nbEdges;
    ifs >> nbJobs >> nbEdges;
    if (nbJobs == 0) {
        throw std::runtime_error("Number of jobs is null");
    }
    std::vector<int> times(nbJobs);
    Graph::Node node;
    for (node = 0; node < nbJobs && ifs; ++node) {
        ifs >> times[node];
        if (times[node] == 0) {
            throw std::runtime_error("times[" + std::to_string(node) + "] is null");
        }
    }
    if (node < nbJobs) {
        throw std::runtime_error("Given less jobs time than jobs");
    }

    DiGraph<int> workflow(nbJobs);
    int u, v, delay;
    int e;
    for (e = 0; e < nbEdges && ifs; ++e) {
        ifs >> u >> v >> delay;
        workflow.addEdge(u, v, delay);
        assert(u < nbJobs);
        assert(v < nbJobs);
    }
    if (e < nbEdges) {
        throw std::runtime_error("Missing informations on communication tasks");
    }
    assert(isDAG(workflow));
    return {workflow, times, _nbPCU};
}

void printMachine(const boost::multi_array<int, 2>& _machine, const int _end) noexcept {
    for (int u = 0; u < _machine.shape()[1]; ++u) {
        for (int t = 0; t <= _end; ++t) {
            if (_machine[u][t] == Scheduling::Idle) {
                std::cout << "***";
            } else {
                std::cout << std::setw(3) << _machine[u][t];
            }
            std::cout << ' ';
        }
        std::cout << '\n';
    }
    std::cout << '\n';
}
void printMachines(const boost::multi_array<int, 2>& _machines, const boost::multi_array<int, 2>& _inNetwork,
    const boost::multi_array<int, 2>& _outNetwork, const int _end) noexcept {
    printMachine(_machines, _end);
    printMachine(_inNetwork, _end);
    printMachine(_outNetwork, _end);
}

} // namespace Scheduling

std::vector<int> getBLevel(const Scheduling::Instance& _inst) noexcept {
    std::vector<int> bLevels = _inst.times;
    const auto reverseWorkflow = _inst.workflow.getReversedGraph();

    const auto topoOrder = getTopologicalOrder(reverseWorkflow);
    for (const auto u : topoOrder) {
        for (const auto v : reverseWorkflow.getNeighbors(u)) {
            bLevels[v] = std::max<int>(bLevels[v], _inst.times[v]
                                                       + bLevels[u] + reverseWorkflow.getEdgeWeight(u, v));
        }
    }
    return bLevels;
}

std::vector<int> getTLevel(const Scheduling::Instance& _inst) noexcept {
    std::vector<int> tLevels(_inst.workflow.getOrder(), 0);
    const auto topoOrder = getTopologicalOrder(_inst.workflow);
    for (const auto u : topoOrder) {
        for (const auto v : _inst.workflow.getNeighbors(u)) {
            tLevels[v] = std::max<int>(tLevels[v], _inst.times[u] + tLevels[u]
                                                       + _inst.workflow.getEdgeWeight(u, v));
        }
    }
    return tLevels;
}

std::vector<int> getBLevel(const Scheduling::Instance& _inst,
    const std::vector<Graph::Node>& _machineAssignment) noexcept {
    std::vector<int> bLevels = _inst.times;
    const auto reverseWorkflow = _inst.workflow.getReversedGraph();

    const auto topoOrder = getTopologicalOrder(reverseWorkflow);
    for (const auto u : topoOrder) {
        for (const auto v : reverseWorkflow.getNeighbors(u)) {
            if (_machineAssignment[u] == _machineAssignment[v]) {
                bLevels[v] = std::max<int>(bLevels[v], _inst.times[v]
                                                           + bLevels[u]);
            } else {
                bLevels[v] = std::max<int>(bLevels[v], _inst.times[v]
                                                           + bLevels[u] + reverseWorkflow.getEdgeWeight(u, v));
            }
        }
    }
    return bLevels;
}

std::vector<int> getTLevel(const Scheduling::Instance& _inst,
    const std::vector<Graph::Node>& _machineAssignment) noexcept {
    std::vector<int> tLevels(_inst.workflow.getOrder(), 0);
    const auto topoOrder = getTopologicalOrder(_inst.workflow);
    for (const auto u : topoOrder) {
        for (const auto v : _inst.workflow.getNeighbors(u)) {
            if (_machineAssignment[u] == _machineAssignment[v]) {
                tLevels[v] = std::max<int>(tLevels[v], _inst.times[u] + tLevels[u]);
            } else {
                tLevels[v] = std::max<int>(tLevels[v], _inst.times[u] + tLevels[u]
                                                           + _inst.workflow.getEdgeWeight(u, v));
            }
        }
    }
    return tLevels;
}

std::vector<int> getPrio(const Scheduling::Instance& _inst,
    const std::vector<Graph::Node>& _machineAssignment) noexcept {
    const auto bLevels = getBLevel(_inst, _machineAssignment);
    const auto tLevels = getTLevel(_inst, _machineAssignment);
    std::vector<int> prio(_inst.workflow.getOrder());
    for (int i = 0; i < prio.size(); ++i) {
        prio[i] = bLevels[i] + tLevels[i];
    }
    return prio;
}

Scheduling::Solution
schedulingFromMachineAssigment(const std::vector<Graph::Node>& _machineAssignment,
    const Scheduling::Instance& _inst) noexcept {

    const std::vector<int> prio = getPrio(_inst, _machineAssignment);
    DBOUT(std::clog << "prio: " << prio << '\n';)
    std::vector<int> minStartTime = getTLevel(_inst, _machineAssignment);
    DBOUT(std::clog << "minStartTime: " << minStartTime << '\n';)

    const auto taskComp = [&](const std::pair<int, Graph::Node>& __lhs, const std::pair<int, Graph::Node>& __rhs) {
        if (__lhs.first == __rhs.first) {
            return prio[__lhs.second] < prio[__rhs.second];
        }
        return __lhs.first > __rhs.first;
    };
    using Server = ProcessorState<decltype(taskComp)>;
    std::vector<Server> machineState(_inst.nbCPU, Server(taskComp));

    auto inDegrees = getInDegrees(_inst.workflow);
    for (int u = 0; u < _inst.workflow.getOrder(); ++u) {
        if (inDegrees[u] == 0) {
            machineState[_machineAssignment[u]].addTask(0, u);
        }
    }

    Scheduling::Solution solution(_inst);

    int scheduledTasks = 0;
    while (scheduledTasks < _inst.workflow.getOrder()) {
        auto serverWithSchedulableTasks = machineState
                                          | ranges::view::remove_if([&](auto&& _machine) { return !_machine.hasReadyTasks(); });

        const auto minMachine = ranges::min_element(serverWithSchedulableTasks,
            [&](auto&& __lhsMachine, auto&& __rhsMachine) {
                if (__lhsMachine.getMinStartTime() == __rhsMachine.getMinStartTime()) {
                    return prio[__lhsMachine.getTop().second] > prio[__rhsMachine.getTop().second];
                } else {
                    return __lhsMachine.getMinStartTime() < __rhsMachine.getMinStartTime();
                }
            });

        assert(minMachine->hasReadyTasks());
        const auto[readyTask, startTime] = minMachine->getTaskAndTime();
        DBOUT(std::clog << "Scheduling " << readyTask << '\n';)
        solution.scheduleCPUTask(readyTask, _machineAssignment[readyTask], startTime);
        minMachine->schedule({readyTask, _inst.times[readyTask]}, startTime);

        for (const auto successor : _inst.workflow.getNeighbors(readyTask)) {
            if (_machineAssignment[readyTask] != _machineAssignment[successor]) { // Different machines -> network delay

                const auto state = solution.findFirstContiguousSlot(
                    {_machineAssignment[readyTask], _machineAssignment[successor]},
                    _inst.workflow.getEdgeWeight(readyTask, successor),
                    startTime + _inst.times[readyTask]);

                assert([&](){
                    if(state.found) {
                        return true;
                    }
                    solution.printScheduling(std::cout);
                    return false;
                }());
                // Place network task
                solution.scheduleCommTask({readyTask, _machineAssignment[readyTask]},
                    {successor, _machineAssignment[successor]}, state.comStart);
                minStartTime[successor] = std::max(minStartTime[successor], state.comStart + _inst.workflow.getEdgeWeight(readyTask, successor));

                if (--inDegrees[successor] == 0) {
                    DBOUT(std::clog << "Pushing task " << successor << " to m=" << _machineAssignment[successor] << " with min start=" << startTime + _inst.times[readyTask] + _inst.workflow.getEdgeWeight(readyTask, successor) << '\n';)
                    machineState[_machineAssignment[successor]].addTask(minStartTime[successor], successor);
                }
            } else { // Same machine -> no delay
                minStartTime[successor] = std::max<int>(minStartTime[successor],
                    startTime + _inst.times[readyTask]);

                if (--inDegrees[successor] == 0) {
                    DBOUT(std::clog << "Pushing task " << successor << " to m=" << _machineAssignment[successor] << " with min start=" << startTime + _inst.times[readyTask] << '\n';)
                    machineState[_machineAssignment[successor]].addTask(minStartTime[successor], successor);
                }
            }
        }
        ++scheduledTasks;
    }
    return solution;
}

std::vector<int> getPrio(const Scheduling::Instance& _inst) noexcept {
    const auto bLevels = getBLevel(_inst);
    const auto tLevels = getTLevel(_inst);
    std::vector<int> prio(_inst.workflow.getOrder());
    for (int i = 0; i < prio.size(); ++i) {
        prio[i] = bLevels[i] + tLevels[i];
    }
    return prio;
}
