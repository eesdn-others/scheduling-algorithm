#include "DSC.hpp"

Scheduling::Solution DSC(const Scheduling::Instance& _inst) {
    /// bLevels[u] = size of the longest path from u to an exit node of the digraph
    std::vector<int> bLevels = getBLevel(_inst);
    DBOUT(std::clog << "bLevels: " << bLevels << '\n';)

    /// ptLevels[u] = size of the longest path from an entry node of the digraph to u
    std::vector<int> ptLevels(_inst.workflow.getOrder(), 0);

    /// clusters of the nodes, at the beginning, every node is in its own cluster
    std::vector<int> cluster(_inst.workflow.getOrder());
    std::generate(cluster.begin(), cluster.end(), [i = 0]() mutable {
        return i++;
    });

    /// Cluster size is equal to the times of the 
    std::vector<int> clusterSize = _inst.times;
    DBOUT(std::clog << "clusterSize: " << clusterSize << '\n';)

    /// Number of task blocking the cluser ???
    std::vector<int> blockedBy(_inst.workflow.getOrder(), 0);

    /// Boolean telling which nodes were examined
    std::vector<bool> examined(_inst.workflow.getOrder(), false);
    
    // blocking[ni] = cId <=> ni is blocking the move to cluster cId
    std::vector<Graph::Node> blocking(_inst.workflow.getOrder(), std::numeric_limits<Graph::Node>::max());

    /// Priority comparator, return true if _u has priority over _v
    const auto prioComp = [&](const Graph::Node _u, const Graph::Node _v) {
        return bLevels[_u] + ptLevels[_u] <= bLevels[_v] + ptLevels[_v];
    };

    using HeapPrio = boost::heap::d_ary_heap<Graph::Node, boost::heap::arity<2>, boost::heap::mutable_<true>, boost::heap::compare<decltype(prioComp)>>;
    /// Heap for the free tasks
    HeapPrio freeList(prioComp);
    std::map<Graph::Node, HeapPrio::handle_type> freeHandle;
    /// Heap for the partially free tasks
    HeapPrio partiallyFreeList(prioComp);
    std::map<Graph::Node, HeapPrio::handle_type> pfreeHandle;

    /// Number of predecessors that are not free
    std::vector<int> incNotFree(_inst.workflow.getOrder());
    for (Graph::Node u = 0; u < _inst.workflow.getOrder(); ++u) {
        incNotFree[u] = _inst.workflow.getInDegree(u);
        // Add free task to the right heap
        if (incNotFree[u] == 0) {
            freeHandle[u] = freeList.push(u);
        }
    }

    while (!freeList.empty()) {
        DBOUT(std::clog << "freeList: " << freeList << '\n';)
        DBOUT(std::clog << "partiallyFreeList: " << partiallyFreeList << '\n';)

        const auto nx = freeList.top();
        freeList.pop();
        // Check if nx was blocking any cluster
        if (blocking[nx] != std::numeric_limits<Graph::Node>::max()) {
            --blockedBy[blocking[nx]];
            blocking[nx] = std::numeric_limits<Graph::Node>::max();
        }
        examined[nx] = true;

        DBOUT(std::clog << "Examining nx: " << nx << '\n';)
        if (ptLevels[nx] > 0) {
            DBOUT(std::clog << "Free prio: " << bLevels[nx] + ptLevels[nx] << '\n';)
            // First check that if a partially free node exists, it does have higher priority
            if (!partiallyFreeList.empty()) {
                const auto ny = partiallyFreeList.top();
                DBOUT(std::clog << "nx prio: " << bLevels[nx] + ptLevels[nx] 
                    << " vs ny prio: " << bLevels[ny] + ptLevels[ny] << '\n';)
                if (bLevels[nx] + ptLevels[nx] < bLevels[ny] + ptLevels[ny]) {
                    DBOUT(std::clog << ny << " has a higher prio\n";)
                    std::vector<Graph::Node> yPredecessors = _inst.workflow.getInNeighbors(ny);

                    // Sort predecessors of ny by decreasing priority
                    assert(yPredecessors.size() > 1);
                    std::sort(yPredecessors.begin(), yPredecessors.end(),
                        [&](const Graph::Node __pred1, const Graph::Node __pred2) {
                        return std::make_tuple(examined[__pred1],
                            ptLevels[__pred1] + _inst.times[__pred1]
                                + _inst.workflow.getEdgeWeight(__pred1, ny))
                            > std::make_tuple(examined[__pred2],
                            ptLevels[__pred2] + _inst.times[__pred2]
                                + _inst.workflow.getEdgeWeight(__pred2, ny));
                    });
                    const auto& n1 = yPredecessors.front();
                    DBOUT(std::clog << "Checking if (" << n1 << ", " << ny << ") can be zeroed. -> "
                        << std::make_tuple(examined[n1],
                            ptLevels[n1] + _inst.times[n1] < ptLevels[ny]) << '\n';)
                    if (examined[n1] && ptLevels[n1] + _inst.times[n1] < ptLevels[ny]) {
                        DBOUT(std::clog << "Blocking move to cluster " << n1 
                            << " with " << ny << '\n';)
                        if (blocking[ny] == std::numeric_limits<Graph::Node>::max()) {
                            blockedBy[cluster[n1]]++;
                            blocking[ny] = n1;
                        }
                    }
                }
            }

            // Try to zero nx with some of its predecessors
            std::vector<Graph::Node> predecessors = _inst.workflow.getInNeighbors(nx);
            DBOUT(std::clog << "Predecessors of" << nx << ": ";
                std::copy(predecessors.begin(), predecessors.end(),
                std::ostream_iterator<Graph::Node>(std::clog, ", "));
                std::clog << '\n';)
            
            // Find the predecessor of nx with the smallest priority
            std::sort(predecessors.begin(), predecessors.end(),
                [&ptLevels, &_inst, nx](auto&& __pred1, auto&& __pred2) {
                DBOUT(std::clog << "Comparing " << __pred1 << " and " << __pred2 << '\n';)
                return ptLevels[__pred1] + _inst.times[__pred1]
                    + _inst.workflow.getEdgeWeight(__pred1, nx)
                    > ptLevels[__pred2] + _inst.times[__pred2]
                    + _inst.workflow.getEdgeWeight(__pred2, nx);
            });
            const auto& n1 = predecessors.front();
            DBOUT(std::clog << "blockedBy[" << n1 << "] " << blockedBy[n1] << '\n';)
            
            // If the cluster of n1 is not blocked, we can zero nx in it
            if (blockedBy[cluster[n1]] == 0) {
                const auto firstNonPossibleZeroing = std::find_if(std::next(predecessors.begin()),
                    predecessors.end(),
                    [&](auto&& _nt) {
                        const auto& childrenOfPred = _inst.workflow.getNeighbors(_nt);
                        if(cluster[n1] != cluster[_nt]) {
                            return childrenOfPred.size() > 1;
                        } else {
                            return false;
                        }
                        /*if(childrenOfPred.size() == 1) {
                            return false;
                        } else {
                            return std::any_of(childrenOfPred.begin(), childrenOfPred.end(),
                            [&](const Graph::Node _v) {return cluster[_v] != cluster[n1];});
                        }*/
                });

                DBOUT(std::clog << "Predecessors: ";
                    std::copy(predecessors.begin(), firstNonPossibleZeroing,
                        std::ostream_iterator<Graph::Node>(std::clog, " "));
                    std::clog << '|';
                    std::copy(firstNonPossibleZeroing, predecessors.end(),
                        std::ostream_iterator<Graph::Node>(std::clog, " "));
                    std::clog << '\n';
                )

                bool successZeroing = false;
                int totalTime = 0; // Why is totalTime 0, shouldn't it be clusterSize[n1] ??
                for (auto ite = predecessors.begin(); ite != firstNonPossibleZeroing; ++ite) {
                    DBOUT(std::clog << std::make_tuple(totalTime, _inst.times[*ite],
                        "<", ptLevels[*ite], _inst.workflow.getEdgeWeight(*ite, nx),
                        _inst.times[*ite]) << '\n';
                    )
                    // If we can zero (*ite, nx) w/o increasing PT
                    if (totalTime + _inst.times[*ite] 
                        < ptLevels[*ite] + _inst.workflow.getEdgeWeight(*ite, nx)
                            + _inst.times[*ite]) {
                        successZeroing = true;
                        totalTime += _inst.times[*ite];
                        DBOUT(std::clog << "Zeroing (" << *ite << ", " << nx << "). Moving "
                            << *ite << " to cluster " << cluster[n1] << ".\n";)

                        // Swap cluser of *ite
                        clusterSize[cluster[*ite]] -= _inst.times[*ite];
                        cluster[*ite] = cluster[n1];
                        clusterSize[cluster[n1]] += _inst.times[*ite];
                    }
                }

                if (successZeroing) {
                    DBOUT(std::clog << "Zeroing (" << n1 << ", " << nx << "). Moving " 
                        << nx << " to cluster " << cluster[n1] << ".\n";
                    )

                    // Swap cluser of nx
                    clusterSize[cluster[nx]] -= _inst.times[nx];
                    cluster[nx] = cluster[n1];
                    clusterSize[cluster[n1]] += _inst.times[nx];
                }
                DBOUT(std::clog << "clusterSize: " << clusterSize << '\n';)
                // Update tlevel[nx] \todo{Can be easily updated during zeroing process}
                ptLevels[nx] = 0;
                for (const auto& pred : predecessors) {
                    if (cluster[pred] == cluster[nx]) {
                        ptLevels[nx] = std::max(ptLevels[nx],
                            ptLevels[pred] + _inst.times[pred]);
                    } else {
                        ptLevels[nx] = std::max<int>(ptLevels[nx],
                            ptLevels[pred] + _inst.times[pred]
                            + _inst.workflow.getEdgeWeight(pred, nx));
                    }
                }
            }
        } else {
            DBOUT(std::clog << "Entry node\n";)
        }

        // Update sucessors
        for (const auto& nj : _inst.workflow.getNeighbors(nx)) {
            ptLevels[nj] = std::max<int>(ptLevels[nj],
                ptLevels[nx] + _inst.times[nx] + _inst.workflow.getEdgeWeight(nx, nj));
            --incNotFree[nj];
            if (incNotFree[nj] == 0) {
                // Remove from partially free list if nx was the last pred to do
                if (pfreeHandle.find(nj) != pfreeHandle.end()) {
                    partiallyFreeList.erase(pfreeHandle.at(nj));
                }
                freeHandle[nj] = freeList.push(nj);
                DBOUT(std::clog << "Pushing " << nj << " to the freeList\n";)
            } else {
                // Add nj to partially free list if not already in
                if (pfreeHandle.find(nj) == pfreeHandle.end()) {
                    pfreeHandle[nj] = partiallyFreeList.push(nj);
                    DBOUT(std::clog << "Pushing " << nj << " to the part_freeList\n";)
                }
            }
        }
        DBOUT(std::clog << "\n\n";)
    }
    DBOUT(std::clog << cluster << '\n';)
    DBOUT(std::clog << clusterSize << '\n';)


    // Cluster are now done, move to machine assignment
    struct Machine {
        int lClock = 0;
        std::vector<int> clusters{};
    };
    std::vector<Machine> machines(_inst.nbCPU);
    std::vector<Machine>::iterator minMachine = machines.begin();

    /// Active clusters, sorted by decreasing size
    std::vector<int> clusters;
    for (int i = 0; i < clusterSize.size(); ++i) {
        if (clusterSize[i] > 0) {
            clusters.push_back(i);
        }
    }
    std::sort(clusters.begin(), clusters.end(), [&](auto&& _t1, auto&& _t2) {
        return clusterSize[_t1] > clusterSize[_t2];
    });

    // Assign to cluster to machine with minimum local clock
    for (const auto& task : clusters) {
        DBOUT(
            for (const auto& machine : machines) {
                std::clog << machine.lClock << ", " << machine.clusters << "\t";
            }
            std::clog << '\n';
        )
        minMachine->lClock += clusterSize[task];
        minMachine->clusters.push_back(task);
        // Get machine with minimum local clock
        minMachine = std::min_element(machines.begin(), machines.end(),
            [&](auto&& _m1, auto&& _m2) {return _m1.lClock < _m2.lClock;});
    }
    DBOUT(
        std::clog << "clusters machine: ";
        for (const auto& machine : machines) {
            std::clog << machine.clusters << "\t";
        }
        std::clog << '\n';
    )

    // Finally, get machine assignment
    std::vector<Graph::Node> machineAssigment(_inst.workflow.getOrder());
    for (int m = 0; m < machines.size(); ++m) {
        for (const auto clust : machines[m].clusters) {
            for (int task = 0; task < _inst.workflow.getOrder(); ++task) {
                if (cluster[task] == clust) {
                    machineAssigment[task] = m;
                }
            }
        }
    }
    DBOUT(std::clog << "machineAssigment: " << machineAssigment << '\n';)
    return schedulingFromMachineAssigment(machineAssigment, _inst);
}
