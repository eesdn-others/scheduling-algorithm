// #define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
// #include "catch.hpp"
#include "DSC.hpp"
#include "Scheduling.hpp"

int main() {
    // DiGraph workflow(7);
    // workflow.addEdge(0, 1, 3 * 2);
    // workflow.addEdge(0, 2, 0.5 * 2);
    // workflow.addEdge(1, 6, 2 * 2);
    // workflow.addEdge(2, 5, 2.5 * 2);
    // workflow.addEdge(3, 5, 4 * 2);
    // workflow.addEdge(4, 5, 1 * 2);
    // workflow.addEdge(5, 6, 2.5 * 2);

    DiGraph<int> workflow1(7);
    workflow1.addEdge(0, 2, 5);
    workflow1.addEdge(0, 3, 4);
    workflow1.addEdge(2, 5, 1);
    workflow1.addEdge(3, 5, 2);
    workflow1.addEdge(1, 3, 1);
    workflow1.addEdge(3, 6, 1);
    workflow1.addEdge(4, 6, 3);

    const Scheduling::Instance inst1(std::move(workflow1), {1, 1, 1, 1, 3, 2, 1}, 2);
    const auto sol = DSC(inst1);
    printMachines(sol.machines, sol.inNetwork, sol.outNetwork, sol.makespan);
    sol.checkSolution(inst1);

    DiGraph<int> workflow2(5);
    workflow2.addEdge(0, 1, 1);
    workflow2.addEdge(0, 2, 1);
    workflow2.addEdge(0, 3, 1);
    workflow2.addEdge(1, 4, 1);
    workflow2.addEdge(2, 4, 1);
    workflow2.addEdge(3, 4, 1);

    const Scheduling::Instance inst2(std::move(workflow2), {1, 2, 2, 2, 1}, 2);

    std::cout << "getTopologicalOrder: " << getTopologicalOrder(inst2.workflow) << '\n';
    std::cout << "bLevels: " << getBLevel(inst2) << '\n';
    std::cout << "tLevels: " << getTLevel(inst2) << '\n';
    std::cout << "Prio: " << getPrio(inst2) << '\n';
}