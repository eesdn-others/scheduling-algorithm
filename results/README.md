# Solving instances

Instance available in the [instances](results/instances) folder can be solved in batch using the [launch.sh](script/launch.sh) script. You just need to call in in the [results](results) folder

```bash
../script/launch.sh
```

You can modify the range of the experiment argument in the file by changing the `CPU_RANGE`, `NETWORK_FACTOR_RANGE`, and `ALGOS` variables.

You can also restrict the name of the instance you consider by changing `$(find ./instances -name *K1s_*)` part of the parallel calls. For example, if you want to only launch the K1 instances with 40 nodes, you can use `$(find ./instances -name *K1_40*)`

# Generating instances

The script [gen.sage](script/gen.sage) can generate three different types of dependency graphs: K1, K2, and random graph.
It requires to have [SageMath](www.sagemath.org) installed on the current machine/

### K1
Assuming that the working directory is ./results/, the command
```bash
sage ../script/gen.sage K1 10 --comCosts uniform --taskCosts uniform
```
will generate a K1 graph with 10 "middle" tasks --- 12 nodes in total --- where all duration (task and communication) are equal to one.

### K2

A K2 graph can be generated with 
```bash
sage ../script/gen.sage K2 {nbNodes} -pn1 {percentage} -p {proba}
```
where percentage represent the percentage of nodes on the first level and proba is the probability to have an edge between tasks on different layers of the graph.

### Random graph

Finally, random connected direct acyclic graph can be generated using the random argument
```bash
sage ../script/gen.sage random {nbNodes} -p {proba}
```
where proba is the probability to have an edge between the two tasks.

## Generating multiple workflows

The script allow us to generate instances with multiple distint workflows: multiple K1s, K2s, randoms or a mix between the three (see below).
```bash
sage ../script/gen.sage [allrands, allK1s, allK2s, workflows] {nbInstances} --nbWorkflow {nbOfWorkflow}
```

For example, if you want to generate an instance with a mix of 100 workflows, use the following command:
```bash
sage ../script/gen.sage workflows 1 --nbWorkflow 100
```

Note that the size of the workflow is currently defined with the _Google distribution_. 
