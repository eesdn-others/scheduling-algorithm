#ifndef LIST_SCHEDULING_HPP
#define LIST_SCHEDULING_HPP

#define BOOST_DISABLE_ASSERTS

#include <iomanip>
#include <vector>

//#include "Matrix.hpp"
#include "Scheduling.hpp"
#include "utility.hpp"
#include <boost/multi_array.hpp>

/**
* \brief {Compute a scheduling using the classical list scheduling with delay} 
* \param _inst Scheduling instance
*/
Scheduling::Solution listDelay(const Scheduling::Instance& _inst);

/**
* \brief {Compute a scheduling using the classical list scheduling} 
* \param _inst Scheduling instance
*/
Scheduling::Solution list(const Scheduling::Instance& _inst);

Scheduling::Solution listNoCapacity(const Scheduling::Instance& _inst);

Scheduling::Solution listScheduling(const Scheduling::Instance& _inst);

#endif
