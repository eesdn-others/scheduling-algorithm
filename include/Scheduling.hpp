#ifndef SCHEDULING_HPP
#define SCHEDULING_HPP
#include <algorithm>
#include <stdexcept>
#include <string>

#include <boost/heap/d_ary_heap.hpp>
#include <boost/heap/priority_queue.hpp>
#include <boost/multi_array.hpp>

#include "DiGraph.hpp"
#include "utility.hpp"

namespace Scheduling {
constexpr int Idle = std::numeric_limits<int>::max(); /// Constant to represents idle time slots

/** 
* \brief The structure for the Scheduling problem
*/
struct Instance {
    Instance(DiGraph<int> _workflow, std::vector<int> _times, const int _nbCPU);
    Instance(const Instance&) = default;
    Instance& operator=(const Instance&) = default;
    Instance(Instance&&) = default;
    Instance& operator=(Instance&&) = default;
    ~Instance() = default;

    int getCPUCost(const int _cpuTask) const {
        return times[_cpuTask];
    }

    DiGraph<int> workflow;             /// The dependency graph
    std::vector<int> times;            /// The CPU times
    int nbCPU;                         /// The number of available CPU
    boost::multi_array<int, 2> edgeId; /// An dictionnary for the communication task IDs
    int lowerBound;                    /// Lower bound on the makespan (sum of CPU task divided by number of machine)
    int upperBound;                    /// Upper bound on the makespan  (sum of CPU task + network task)
};

/**
* \brief The Solution structure for the Scheduling problem
*/
struct Solution {
    struct TaskPlacement {
        int task = std::numeric_limits<int>::max();
        int machine = std::numeric_limits<int>::max();
    };

    struct TimedTaskPlacement {
        int task = std::numeric_limits<int>::max();
        int machine = std::numeric_limits<int>::max();
        int time = std::numeric_limits<int>::max();

        bool isValid() const {
            return time != std::numeric_limits<int>::max()
                   && machine != std::numeric_limits<int>::max();
        }
    };

    /**
    * \brief{Structure used for representing contiguous slot in a scheduling}
    */
    struct ContiguousState {
        bool found;
        int comStart;
        int nbContiguousSlotAtEnd;
    };

    struct Task {
        int id;
        int duration;
    };

    const Scheduling::Instance* inst;      /// Scheduling instance
    int makespan = 0;                      /// The makespan of the scheduling
    std::vector<std::map<int, Task>> machines;   /// Scheduling of the CPU
    std::vector<std::map<int, Task>> inNetwork;  /// Scheduling of the incoming network machines
    std::vector<std::map<int, Task>> outNetwork; /// Scheduling of the outgoing network machines

    Solution(const Scheduling::Instance& _inst);

    Solution reverse(const Instance& _inst) const;

    void scheduleCPUTask(const int _task, const int _machine, const int _time) noexcept;

    void scheduleCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
        const int _time) noexcept;

    void revertScheduleCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
        const int _time) noexcept;

    /** 
    * \brief{Return either the start of a block of _size contiguous slots or the size of the final contiguous slot on both network machine}
    */
    ContiguousState findFirstContiguousSlot(const std::pair<int, int>& _networkMachines,
        const int _size, const int _start, const int _end);
    ContiguousState findFirstContiguousSlot(const std::pair<int, int>& _networkMachines,
        const int _size, const int _start);

    /**
	*	\brief Save the makespan and the scheduling into a file
		\param _filename The filename of the file to save the solution
	*	\sa Solution::printScheduling 
	*/
    void save(const std::string& _filename) const;

    /**
    * \brief Print the scheduling of the machine in a compact form
    * For each scheduled task, print the 'task size' of the size of the task is bigger than 1, or just 'task' otherwise
    * \sa Solution::save
    */
    template<typename FormattingFunction>
    void printScheduling(std::ostream& _out, const std::vector<std::map<int, Task>>& _schedules,
        FormattingFunction&& _func) const noexcept {   
    for (const auto& schedule : _schedules) {
        for(const auto& [time, task] : schedule) {
            _out << time << ',' << _func(task.id) << ',' << task.duration << '\t';
        }
        _out << '\n';
    }
}

    /**
	* \brief Print the scheduling of all machines in a compact form
	* \sa Solution::printScheduling
	*/
    void printScheduling(std::ostream& _out) const noexcept;

    /**
    *   \brief{Return the placement of the tasks (id, machine and starting time)}
    *   \sa TaskPlacement
    */
    std::vector<TimedTaskPlacement> getTaskPlacements(const std::vector<std::map<int, Solution::Task>>& _machine, int _nbTasks) const noexcept;

    /**
    *	\brief Check if the solution is valid for the Instance _inst
	*	\return true if the solution is valid, false otherwise
    */
    bool isValid() const noexcept;
    Solution getReverse() const noexcept;

  private:
    void setCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
        const int _time, int _id) noexcept;
    void removeCommTask(const TaskPlacement& _task1, const TaskPlacement& _task2,
    const int _time) noexcept;
};


void printSchedulingTex(const Scheduling::Instance& _inst, const Scheduling::Solution& _sol);

/**
*	\brief{Return an Scheduling::Instance from the file _filename}
*	\param _filename The name of the file containing the instance
	\exception std::runtime_error 
*/
Scheduling::Instance readFromFile(const std::string& _filename, int _nbPCU);

/**
*	\brief Print the scheduling of machines on the standard output from the start to the specified end
*	\param _machine The machine to print
*	\param _end The end of the period to print
*	\sa printMachines
*/
void printMachine(const std::vector<std::map<int, Solution::Task>>& _machine, const int _end) noexcept;

/*
*	\brief{Print the CPU, IN and OUT scheduling of a server to the standard output}
*	\param _machines The CPUs of the servers
*	\param_ inNetwork The incoming network machines of the servers
*	\param _outNetwork The outgoing network machines of the servers
*	\param _end The end of the period to print
*	\sa printMachine
*/
void printMachines(const std::vector<std::map<int, Solution::Task>>& _machines, const std::vector<std::map<int, Solution::Task>>& _inNetwork,
    const std::vector<std::map<int, Solution::Task>>& _outNetwork, const int _end) noexcept;

} // namespace Scheduling

/**
*	\brief Return, for all nodes of a workflow, the size of the longest path to an entry node
*	$ blevel(u) = \max_{v \in N^-(u)}(blevel(v) + \comcost{u}{v}) $
*/
std::vector<int> getBLevel(const Scheduling::Instance& _inst) noexcept;

/**
*	\brief Return, for all nodes of a workflow, the size of the longest path to an exit node
*	$ tlevel(u) = \max_{v \in N^-(u)}(\cpucost{u} + tlevel(v) + \comcost{u}{v}) $
*/
std::vector<int> getTLevel(const Scheduling::Instance& _inst) noexcept;

std::vector<int> getBLevel(const Scheduling::Instance& _inst,
    const std::vector<Graph::Node>& _machineAssignment) noexcept;

std::vector<int> getTLevel(const Scheduling::Instance& _inst,
    const std::vector<Graph::Node>& _machineAssignment) noexcept;

/**
*	\brief Returns the size of the longest path the node belongs
*/
std::vector<int> getPrio(const Scheduling::Instance& _inst) noexcept;

/**
*	\brief Returns the size of the longest path the node belongs
*/
std::vector<int> getPrio(const Scheduling::Instance& _inst,
    const std::vector<Graph::Node>& _machineAssignment) noexcept;

/**
* \brief{Represent a server}
* \sa{schedulingFromMachineAssigment}
*/
template <typename Comparator>
class ProcessorState {
  public:
    using TimePlacement = std::pair<int, Graph::Node>;

  public:
    ProcessorState(const Comparator& _comparator)
        : readyList(_comparator) {}

    ProcessorState(const ProcessorState&) = default;
    ProcessorState& operator=(const ProcessorState&) = default;
    ProcessorState(ProcessorState&&) = default;
    ProcessorState& operator=(ProcessorState&&) = default;
    ~ProcessorState() = default;

    void addTask(const int _time, const int _task) {
        readyList.emplace(_time, _task);
    }

    /*
    * \brief{Return the first that can be scheduled on the machine and its starting time}
    */
    std::pair<int, int> getTaskAndTime() const noexcept {
        return {readyList.top().second, std::max(firstFreeSlot, readyList.top().first)};
    }

    void schedule(std::pair<int, int> _task, int _startTime) {
        assert(_task.first == getTop().second);
        readyList.pop();

        tasks.emplace_back(_startTime, _task.first);
        firstFreeSlot = _startTime + _task.second;
    }

    bool hasReadyTasks() const {
        return !readyList.empty();
    }

    const TimePlacement& getTop() const {
        return readyList.top();
    }

    int getMinStartTime() const noexcept {
        return std::max(firstFreeSlot, getTop().first);
    }

  private:
    using ReadyList = typename boost::heap::priority_queue<TimePlacement, boost::heap::compare<Comparator>>;
    int firstFreeSlot = 0;
    ReadyList readyList;
    std::vector<std::pair<int, int>> tasks; /// Pair of start time and taskID
};

/**
* \brief{Given a machine assignment, schedule the instance using the following algorithm}
* \details{@TODO}
*/

Scheduling::Solution
schedulingFromMachineAssigment(const std::vector<Graph::Node>& _machineAssignment,
    const Scheduling::Instance& _inst) noexcept;

#endif
