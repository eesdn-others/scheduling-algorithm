#ifndef SCHEDULING_MASTER
#define SCHEDULING_MASTER

#include "Matrix.hpp"
#include "Scheduling.hpp"
#include "utility.hpp"

namespace Scheduling {
class SchedulingMaster {
  public:
    explicit SchedulingMaster(const Scheduling::Instance& /*_inst*/);
    SchedulingMaster(const SchedulingMaster&) = default;
    SchedulingMaster& operator=(const SchedulingMaster&) = default;
    SchedulingMaster(SchedulingMaster&&) = default;
    SchedulingMaster& operator=(SchedulingMaster&&) = default;
    ~SchedulingMaster() = default;

    /* 
	* Returns the first contiguous slot of size _size between machine _k1 and _k2 for t in [_start, _end[
    */
    Scheduling::Solution solve();

    std::tuple<bool, int, int> sortValue(const std::pair<int, int>& __p);

    void buildPairsJobMachines();

  private:
    Scheduling::Instance const* inst;
    int maxTime;
    Solution m_solution;
    std::vector<int> machineCompletion;
    std::vector<int> taskStarts;
    std::vector<int> taskAssignment;
    boost::multi_array<int, 2> minStartTime;

    // Master
    std::vector<int> inDegrees;
    std::vector<int> longestBranches;
    std::vector<int> parents;
    std::vector<bool> isMaster;
    std::vector<int> sumOfComsToMaster;
    boost::multi_array<std::vector<int>, 2> exteMachinePred;

    std::vector<std::pair<int, int>> L = std::vector<std::pair<int, int>>();
    std::vector<int> A = std::vector<int>();
    int currentT = 0;
};
} //namespace Scheduling

#endif
