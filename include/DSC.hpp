#include "boost/multi_array.hpp"
#include "utility.hpp"

#include <Scheduling.hpp>
#include <iterator>
#include <map>

template <typename T, typename... Options>
std::ostream& operator<<(std::ostream& _out, const boost::heap::d_ary_heap<T, Options...>& _heap) {
    _out << "{";
    std::copy(_heap.ordered_begin(), _heap.ordered_end(), std::ostream_iterator<Graph::Node>(_out, ", "));
    return _out << "}";
}

Scheduling::Solution DSC(const Scheduling::Instance& _inst);
